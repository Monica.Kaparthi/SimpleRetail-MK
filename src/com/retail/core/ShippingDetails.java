package com.retail.core;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
public class ShippingDetails {

	public static void main(String[] args) {

		Products prod= new Products();

		SimpleRetailFactory factory= new SimpleRetailFactory();

		List<Item> itemslist= prod.getItemsList();

	//	List<List<Item>> itemslist1= Batch.chunk(itemslist, 6);




		String timeStamp = new SimpleDateFormat("yyyy-MM-dd:HH:mm.ss").format(new Date());


		System.out.println("\n\n****SHIPMENT REPORT****                       \t\t\tDate:"+timeStamp+
				"\n\n\n"+"UPC"+"\t\t"+"Description"+"\t\t\t\t\t\t"+"Price"+"\t\t\t"+"Weight"+"\t\t\t"+"ShipMethod"+"\t\t"
				+"Shipping Cost"+"\n");


		//for(List<Item> item: itemslist1) {
			double totalshippingCost = 0;
			for(Item str: itemslist){
				ShippingCost shipcost= factory.checkMethod(str.getShippingMethod());
				System.out.println(str.getUpc()+"\t"+str.getDescription()+"\t\t\t$"+str.getPrice()+"\t\t\t"+str.getWeight()+"\t\t\t"+str.getShippingMethod()+"\t\t\t\t$"+shipcost.costCalculations(str));

				totalshippingCost+=shipcost.costCalculations(str);
				DecimalFormat twoDForm = new DecimalFormat("#.##");
				totalshippingCost=Double.valueOf(twoDForm.format(totalshippingCost));
			}
			System.out.println("\n"+"TOTAL SHIPPING COST:"+"\t$"+totalshippingCost+"\n\n");
		



	}




}


