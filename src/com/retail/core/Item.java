package com.retail.core;


public class Item implements Comparable<Item>{

	private String description;
	private double price;
	private double weight;
	private String shippingMethod;
	private long upc;


	/* POJO class*/

	public Item(String description,double price,double weight, String shippingMethod,long upc)
	{
		this.description=description;
		this.price=price;
		this.weight= weight;
		this.shippingMethod= shippingMethod;
		this.upc= upc;
	}



	public long getUpc() {
		return upc;
	}
	public void setUpc(long upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}



	@Override
	public int compareTo(Item item) {

		if ((Long) this.getUpc() < (Long) item.getUpc())
			return -1;
		if (((Long)this.getUpc()).equals((Long)item.getUpc()))
			return 0;

		return 1;


	}




	//	@Override
	//    public String toString() {
	//        return "[dec="+ description+",price="+price+",weight="+weight+",shippingMethod="+shippingMethod+",upc="+upc+"]";
	//    }
	//	





}
