package com.retail.core;

public class SimpleRetailFactory {
	
	public ShippingCost checkMethod(String method)
	{
		if(method.equalsIgnoreCase("AIR"))
			
		{
			
			return new AirShippingMethod();
		}
		
		else if (method.equalsIgnoreCase("GROUND"))
		{
		
		return new GroundShippingMethod();
		}
		
		else
		{
			throw new IllegalArgumentException("Shipping method should be either 'AIR' or 'GROUND'");
		}
		
		
	}

}
