package com.retail.core;

import java.text.DecimalFormat;

public class AirShippingMethod implements ShippingCost{

	/* Method to calculate Air shipping costs*/
	

	@Override
	public double costCalculations(Item it) {

		double cost = 0;
		double price=it.getPrice();
		double weight=it.getWeight();

		long upc=it.getUpc();
		int number=(int) ((upc%100)/10);
		double totalcost;

		cost=(weight*number);

		totalcost= cost+ ((float)3)*price/100;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		totalcost=Double.valueOf(twoDForm.format(totalcost));
		return totalcost;
	}

}
