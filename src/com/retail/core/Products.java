package com.retail.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Products {
	
	public List<Item> getItemsList(){
	ArrayList<Item> itemslist=new ArrayList<Item>();
	itemslist.add(new Item("CD � Pink Floyd, Dark Side Of The Moon",19.99,0.58,"AIR",567321101987L));
	itemslist.add(new Item("CD � Beatles, Abbey Road\t",17.99,0.61,"GROUND",567321101986L));
	itemslist.add(new Item("CD � Queen, A Night at the Opera",20.49,0.55,"AIR",567321101985L));
	itemslist.add(new Item("CD � Michael Jackson, Thriller\t",23.88,0.50,"GROUND",567321101984L));
	itemslist.add(new Item("iPhone - Waterproof Case\t",9.75,0.73,"AIR",467321101899L));
	itemslist.add(new Item("iPhone -  Headphones\t\t",17.25,3.21,"GROUND",477321101878L));
	itemslist.add(new Item("iPhone -  Charger\t\t",17.25,3.21,"GROUND",477321101878L));
	itemslist.add(new Item("iPhone -  case\t\t\t",17.25,3.21,"GROUND",477321101878L));
	itemslist.add(new Item("iPhone -  screenguard\t\t",17.25,3.21,"GROUND",477321101878L));
	
	
	/*Sorting the items in ascending order*/
	Collections.sort(itemslist);
	return itemslist;
	
	
	}

}
