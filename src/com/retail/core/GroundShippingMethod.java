package com.retail.core;

import java.text.DecimalFormat;

public class GroundShippingMethod implements ShippingCost{
	/* Method to calculate Ground shipping costs*/
	@Override
	public double costCalculations(Item it) {
		 double cost = 0;
		 double price=it.getPrice();
		 double weight=it.getWeight();
		
		 
		 double totalcost;
		
		
		cost= weight*2.5;
		
		totalcost= cost+ ((float)3)*price/100;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		totalcost=Double.valueOf(twoDForm.format(totalcost));
		return totalcost;
		
	}

}
